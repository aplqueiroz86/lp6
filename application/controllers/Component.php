<?php

    defined('BASEPATH') OR exit ('No direct script acess allowed');

    class Component extends CI_Controller{
        
        public function panel(){
            $this->load->view('common/header');

            $this->load->model('ComponentModel', 'cpm'); //CPM = apelido de ComponentModel para não precisar escrever ComponentModel todas as vezes
            $data['panels'] = $this->cpm->getPanelList();
            $this->load->view('component/panel', $data);

            $this->load->view('common/footer');
        }

        public function table(){
            $this->load->view('common/header');

            $this->load->model('ComponentModel', 'cpm'); //CPM = apelido de ComponentModel para não precisar escrever ComponentModel todas as vezes
            $data['table'] = $this->cpm->getTable();
            $this->load->view('component/table', $data);

            $this->load->view('common/footer');
        }
    }

?>