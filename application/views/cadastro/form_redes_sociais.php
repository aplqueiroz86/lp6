<div class="container mt-3">
    <div class="card">
        <div class="card-header">
            <h4>Redes Sociais</h4>
        </div>
        <div class="card-body">
               
                <div class="form-row">
                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('facebook')?>" class="form-control" id="facebook" name="facebook" placeholder="Facebook..">
                        <label for="facebook" >Facebook</label>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('twitter')?>" class="form-control" id="twitter" name="twitter" placeholder="Twitter...">
                        <label for="twitter">Twitter</label>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('instagram')?>" class="form-control" id="instagram" name="instagram" placeholder="Instagram...">
                        <label for="instagram" >Instagram</label>
                    </div>
                    </div>

                    <div class="col-md-6">
                    <div class="md-form form-group">
                        <input type="text" value="<?= set_value('linkedin')?>" class="form-control" id="linkedin" name="linkedin" placeholder="Linkedin...">
                        <label for="linkedin">Linkedin</label>
                    </div>
                    </div>
                </div>
                
                <button class="btn btn-info my-4 btn-block" type="submit">Enviar</button>
            </form>
    </div></div>
</div>

