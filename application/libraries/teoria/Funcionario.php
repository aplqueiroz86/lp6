<?php

include_once 'Pessoa.php'; 


class Funcionario extends Pessoa{

    private $setor;
    private $funcao;

    function __contruct($nome,$idade,$funcao){
        parent::construct($nome,$idade);
        $this->funcao = $funcao;

    }

    public function trabalhar(){
        echo 'trabalhando muito, das seis às seis!<br>';

    }

    public function limiteLivros(){
        return 4;
    }

    public function prazoDeEntrega(){
        return 14;
    }


}