<?php

include_once 'VendasListener.php';

class Estoque implements VendasListener{

    private $data;

    function __construct($data){
        $this->data = $data; 
    }


    public function venda_realizada($produto){
        $dp =$produto->departamento;
        $nome = $produto->nome;
        $preco = $produto->preco;
        $data = $produto->data;
        $this->solicita_reposicao($nome,$dp,$preco,$data);
    }

    private function solicita_reposicao($nome,$dp,$preco,$data){
        echo "<br>Na data de: $data o Produto: $nome foi vendido no valor de $preco pelo Departamento: $dp<br>";
        echo "<br>Solicitamos a reposição do produto: $nome do Departamento: $dp<br>";
    }


    public function compra_realizada($produto){}


}