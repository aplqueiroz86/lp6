<?php
    //defined('BASEPATH') OR exit('No direct script access allowed');
    include_once 'Validator.php';
    
    class EnderecoValidator extends Validator{

        public function validate(){
            $this->form_validation->set_rules('tipo_logradouro','Tipo do Logradouro','required|min_length[3]|max_length[100]');
            $this->form_validation->set_rules('nome_logradouro','Nome do Logradouro','required|min_length[2]|max_length[100]');
            $this->form_validation->set_rules('numero','Número','required|is_natural_no_zero|less_than[30000]');
            $this->form_validation->set_rules('complemento','Complemento','min_length[3]|max_length[20]');
            $this->form_validation->set_rules('cep','CEP','required|exact_length[9]');
            $this->form_validation->set_rules('cidade','Cidade','required|max_length[30]');
            $this->form_validation->set_rules('estado','Estado','required|min_length[2]|max_length[20]');

           
        }

        public function getData(){
            $data['tipo_logradouro'] = $this->input->post('tipo_logradouro');
            $data['nome_logradouro'] = $this->input->post('nome_logradouro');
            $data['numero']          = $this->input->post('numero');
            $data['complemento']     = $this->input->post('complemento');
            $data['cep']             = $this->input->post('cep');
            $data['cidade']          = $this->input->post('cidade');
            $data['estado']          = $this->input->post('estado');
            return $data;
        }
    }

/* Modo manual de acessar os dados do codeIgniter
        private $ci;
        function __contruct(){
            $ci = & get_instance();
            $this->db = $ci->db;
        }
        */