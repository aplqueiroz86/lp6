<?php
  include_once 'DAO.php';

    class RedesSociais extends DAO{


        function __construct(){
            parent::__construct('redes_sociais');
        }

        // sobrescrita de método: cria na classe filha um método 
        //que tem o mesmo nome de outro definido na classe pai
        //isso é feito para especializar algum comportamento
        //especializar comportamento = é fazer a mesma coisa que o pai faz com algum detalhe diferente
        //aqui está incluindo o id_pessoa, coisa que não tem na DAO
        public function salvar($data, $id_pessoa = 0){
            $data['id_pessoa'] = $id_pessoa;
            parent::salvar($data);
        }

    }


