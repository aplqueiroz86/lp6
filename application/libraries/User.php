<?php
    //nome da classe sempre é o mesmo do arquivo
    class User{
        
        //Informações obrigatorias
        private $nome;
        private $sobrenome;
        private $email;
        private $senha;

        //Informações opcionais
        private $telefone;
        
        private $db;

        function __construct($nome = null ,$sobrenome = null ,$email = null ,$senha = null ){
            //echo "Construtor: $nome,$sobrenome,$email,$senha<br>";
            $this->nome = $nome;
            $this->sobrenome = $sobrenome;
            $this->email = $email;
            $this->nome = $nome;


            //Acesso ao banco de dados pelo CI
            //Não é pra entender, só copiar
            $ci = &get_instance();
            $this->db = $ci->db;

        }

        public function setTelefone($telefone){
            $this->telefone = $telefone;
        }

        public function save(){
            $sql = "INSERT INTO user(nome,email,senha,sobrenome,telefone)
                    VALUES('$this->nome','$this->email','$this->senha','$this->sobrenome','$this->telefone')";
            $this->db->query($sql);
        }
        
        /**
         * Obtém a lista de todos os usuarios cadastrados
         * @return associative array
         */
        public function getAll(){
            $sql = "SELECT * FROM user";
            $res = $this->db->query($sql);
            return $res->result_array();
        }

        public function getById($id){
            $rs = $this->db->get_where('user', "id = $id");
            return $rs->row_array();
        }
        public function update($data, $id){
            $this->db->update('user',$data , "id = $id");
            return $this->db->affected_rows();
        }
        
        public function delete($id){
            $this->db->delete('user',"id = $id");
        }
        
    }
    
?>